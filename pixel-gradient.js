document.querySelectorAll("canvas.pixel-gradient").forEach((canvas) => {
  const ctx = canvas.getContext("2d");
  const gradient = ctx.createLinearGradient(0, 0, canvas.width, canvas.height);

  const gradientValues = [
    "#CC99C9",
    "#9EC1CF",
    "#9EE09E",
    "#FDFD97",
    "#FEB144",
    "#FF6663"
  ];
  gradientValues.forEach((value, index) => {
    const stopPosition = (1 / gradientValues.length) * (index + 1);
    gradient.addColorStop(stopPosition, value);
  });

  ctx.fillStyle = gradient;
  ctx.fillRect(0, 0, canvas.width, canvas.height);
});
