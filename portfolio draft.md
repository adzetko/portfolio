# Maxim Hohengarten #
## Web developer and graphic designer ##
24 years old • France (Haute-Loire)


## Projects ##

> *2022*

### **https://multiplex-insoumis.fr** ###

**Concept:** chronological streamers list for an event

**My part:** All CSS and some components (donation counter and bar)

[comment]: <> (add a demo video/offline of the responsive)

> *2021*

### **https://lesdieuxdudev.lecoledunumerique.fr/jeux/escape-ball/** ###
**Concept:** learning how to dev in a team (usage of git), brainstorming a video game for a one month deadline
**My part:** character design and physics, input management
**source code:** 

### **https://aperibrive.fr/** ###
**Concept:**
**My part:**

## Skills ##

CSS enjoyer

### Web Dev ###

### Graphic Designer ###

### Writer ###
